# References:
# https://docs.python.org/2/library/logging.html
# https://docs.python.org/2/library/logging.handlers.html

import logging
import subprocess
import time

FORMAT_STRING = '[%(filename)s:%(lineno)s:%(funcName)s] %(message)s'

# The following configures the logging system to use a format string for the handlers
# that will prepend log messages with the filename, line number, and function name
# where the log message was generated.
#
# Note that the default basic configuration utilizes a StreamHandler that logs
# to standard output.
logging.basicConfig(format=FORMAT_STRING)

# The logging package supports logger hierarchies (parent/child relationships). All one
# needs to know to follow this example is that we are going to acquire what's referred to
# as the "root logger". The string being "root" doesn't actually lead to the root logger
# being returned -- we coud have called it "a". getLogger knows it is a root logger because
# the string contains no dots. Acquiring a child logger would involve a name argument like
# "a.b".
logger = logging.getLogger('root')

# The following sets the threshold for the acquired logger.
# I will explain this concept on the next slide.
logger.setLevel(logging.DEBUG)

while True:
    logger.info(subprocess.check_output('date').strip())
    time.sleep(2)
