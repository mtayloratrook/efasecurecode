#!/usr/bin/python

from time import sleep
from threading import Thread
import socket
import sys
import subprocess
import ssl

class AdminServer(object):
    def __init__(self, command, prompt):
        self.command = command
        self.prompt = prompt

        self.s_main = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.conn_main = None

    def start(self):
        threads = []
        try:
            thread1 = Thread(target=self.listen_for_standard_connections)
            thread1.daemon = True
            threads.append(thread1)
            for thread in threads:
                thread.start()
            while 1:
                sleep(3)
                pass
        except KeyboardInterrupt:
            print 'Ctrl C pressed...'
            print 'Closing sockets...'
            try:
                self.conn_main.close()
            except AttributeError as msg:
                pass
            self.s_main.close()
            self.s_main = None
            print 'Socket closed'
            print 'Exiting...'

    def run_command(self, parameters):
        results = ""
        statement = self.command + " " + parameters

        if ';' in parameters or '&&' in parameters or '|' in parameters:
            statement = "echo Shell syntax is not allow in command. Please enter a new command with no special characters"

        results = subprocess.Popen(statement, shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE).stdout.read()

        return results

    def listen_for_connections(self, port, my_socket, my_connection, command):
        host = ''

        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        try:
            my_socket.bind((host, port))
        except socket.error as msg:
            print 'Bind failed. Error: %s' % msg.strerror
            sys.exit

        my_socket.listen(10)
        print 'Socket now listening on port %s' % port

        my_connection, secure_connection, addr = (None, None, None)

        while my_socket:
            try:
                print 'Waiting for accept'
                my_connection, addr = my_socket.accept()
                print 'Securing connection'
                secure_connection = ssl.wrap_socket(my_connection,
                                                    server_side=True,
                                                    ssl_version=ssl.PROTOCOL_SSLv3,
                                                    certfile="server.crt",
                                                    keyfile="server.key")
                print 'Handling new connection'
                if secure_connection:
                    parameters = ''
                    results = ''

                    while parameters != 'exit':
                        print 'Connected with ' + addr[0] + ':' + str(addr)

                        secure_connection.send(results + '\n\n' + self.prompt + ", or type 'exit' to quit: ")
                        parameters = secure_connection.recv(100)
                        parameters = parameters.strip()
                        if parameters != 'exit':
                            results = command(parameters)
            except socket.error as msg:
                print 'Connection failed. Error: %s' % msg.strerror
            finally:
                try:
                    secure_connection.close()
                except:
                    pass
                if addr:
                    print 'Disconnected with ' + addr[0] + ':' + str(addr)

    def listen_for_standard_connections(self):
        self.listen_for_connections(8888, self.s_main, self.conn_main, self.run_command)

class AdminIfconfigServer(AdminServer):
    def __init__(self):
        super(AdminIfconfigServer, self).__init__("ifconfig", "Please enter an interface name")

if __name__ == '__main__':
    server = AdminIfconfigServer()
    server.start()
