#!/usr/bin/python

from time import sleep
from threading import Thread
import socket
import sys
import subprocess
import ssl
from user_authentication import UserAuthentication
from secure_connection import SecureConnection

class AdminServer(object):
    def __init__(self, command, prompt):
        self.command = command
        self.prompt = prompt

    def start(self):
        threads = []
        try:
            thread1 = Thread(target=self.listen_for_standard_connections)
            thread1.daemon = True
            threads.append(thread1)
            for thread in threads:
                thread.start()
            while 1:
                sleep(3)
                pass
        except KeyboardInterrupt:
            print 'Ctrl C pressed...'
            print 'Closing sockets...'
            if self.secure_connection:
                self.secure_connection.close()
                self.secure_connection = None
            print 'Socket closed'
            print 'Exiting...'

    def run_command(self, parameters):
        results = ""
        statement = self.command + " " + parameters

        if ';' in parameters or '&&' in parameters or '|' in parameters:
            statement = "echo Shell syntax is not allow in command. Please enter a new command with no special characters"

        results = subprocess.Popen(statement, shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE).stdout.read()

        return results

    def listen_for_connections(self, port, command):
        host = ''

        self.secure_connection, addr = (None, None)
        self.secure_connection = SecureConnection('localhost', port)

        while self.secure_connection:
            try:
                self.secure_connection.wait_for_connection()

                parameters = ''
                results = ''

                print 'Connected with ' + self.secure_connection.client_address[0] + ':' + str(self.secure_connection.client_address)
               
                if self.authenticate_login_credentials() != True:
                    parameters = 'exit'
                    self.secure_connection.send_user_text("Error: Invalid username or password")
                    self.secure_connection.close()

                while parameters != 'exit':
                    user_input = self.secure_connection.prompt_user_with_text(results + '\n\n' + self.prompt + ", or type 'exit' to quit: ")
                    
                    if user_input == 'exit':
                        break

                    results = command(user_input)

            except socket.error as msg:
                print 'Connection failed. Error: %s' % msg.strerror
            except Exception as msg:
                print 'bar'
                print 'Generic error while starting a secure connection. Error: %s' % msg
            finally:
                try:
                    self.secure_connection.close()
                except:
                    pass
                if self.secure_connection.client_address:
                    print 'Disconnected with ' + self.secure_connection.client_address[0] + ':' + str(self.secure_connection.client_address)

    def listen_for_standard_connections(self):
        self.listen_for_connections(8888, self.run_command)

    def authenticate_login_credentials(self):
        username = self.secure_connection.prompt_user_with_text("Username: ")
        password = self.secure_connection.prompt_user_with_text("Password: ")

        self.user_authentication = UserAuthentication()
        self.user_authentication.connect('localhost', 'root', 'password', 'service')

        return self.user_authentication.verify_credentials(username, password) 

class AdminIfconfigServer(AdminServer):
    def __init__(self):
        super(AdminIfconfigServer, self).__init__("ifconfig", "Please enter an interface name")

if __name__ == '__main__':
    server = AdminIfconfigServer()
    server.start()
    
