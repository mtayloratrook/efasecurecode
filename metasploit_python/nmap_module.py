import sys
import os
import subprocess
import ipaddress
import xlsxwriter

import socket


class nmap_target(object):

   def __init__(self, target_ip, cidr=False):

        self.target_ip = target_ip

        if cidr is not False:
            self.cidr = cidr

   def get_local_ip(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(('8.8.8.8', 80))
        return s.getsockname()[0]

   def get_target_ip(self):

       return self.target_ip

   def get_target_range(self):

       if self.cidr:
           return ipaddress.ip_network(unicode(self.target_ip + self.cidr))

   def return_nmap_results(self):
        """nmap 127.0.0.1"""
        statement = "nmap %s" % self.target_ip
        results = subprocess.Popen(statement, shell=True, stderr=subprocess.PIPE,
                                  stdout=subprocess.PIPE).stdout.read()
        if 'Host seems down' in results:
            return None
        return results



if __name__ == '__main__':

#    a = nmap_target('127.0.0.1')
#    print a.get_target_ip()
#    print a.return_nmap_results()
    output_dict = {}
    print get_lan_ip()
    cidr_nmap = nmap_target('10.211.55.0', '/24')
    for address in cidr_nmap.get_target_range():
        r = nmap_target(address).return_nmap_results()
        if r is not None:
            output_dict[address] = r
        elif r is None:
            print '%s is Down!!' % address

#        print output_dict
#        sys.exit()


    if len(output_dict.keys()) > 0:
        workbook = xlsxwriter.Workbook('nmap.xlsx')
        worksheet = workbook.add_worksheet()
        row = 1
        for address, output in output_dict.items():
            print address, output
            worksheet.write(row, 1, unicode(address))
            worksheet.write(row, 2, unicode(output))
            row += 1
        workbook.close()
